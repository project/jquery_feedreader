This module creates a block that:
- Loads the contents of a feed (from the current site!) through (JQuery and AJAX)
- Starts to scroll the feed items up, one at a time, through the block (JQuery

It has several configuration options (height, feed, max items, text length, scroll speed, and scroll pause).

As it is, it would require some CSS work to use in production.

I'm releasing it as a beta because I've satisfied my desire to work on it, but it's not 100% quality. If you have patches, or want to maintain the project, let me know. I probably won't fix many bugs on this one.

If you're looking for similar or complete functionality like this, this is a great place to start custom coding!

Best wishes, Mike